# RebelsAssignment (API)
##  Short descreption about the framework :
I created the framework in Java as a **Cucumber/BDD** framework along with Gherkin language in feature files. I used **POM(Page Object Model)** design pattern.  I automated all service’s endpoints in API server: https://petstore.swagger.io/ inside the CI pipeline to test the Petstore. I covered positive and negative scenarios. I attached Html reports to Gitlab jobs as artifacts. 

### Tools used :

Tools | Purpose
------------ | -------------
Java | Main language
Maven | Build management tool to manage dependencies
Intellij | IDE
Cucumber | BDD Framework (Added as a dependency and as a plugin)
Gherkin | Used in feature files for scenarios (Added as plugin in IDE )
JUnit | For assertions and annotations
REST Assured | For Rest API testing
Gitlab CI | CI tool to run framework in pipeline
Gson | Library to convert between JSON strings and Java objects.
Git&Gitlab| Version Control System
Maven cucumber reporting | Generate reports
Maven surefire plugin | To manage runner classes/execute tests


### Design :
#### Page Object Model Design Pattern

Design of the framework is POM and Cucumber Behavior Driven Development framework. 

* **pojos**
   - Basically I create POJO Java Classes in **pojos** package and each class represents an object/Json string in the Rest API.  
   - I store all constructors,getters and setters in each class and I utilize them in Step Definitions.  

* **runners**
   - Runner classes are where I execute testing via Cucumber Options. It includes **CukesRunner** and **FailedRunner**(to run failed cases) classes.
   - I am holding and managing some cucumber option features like **plugin**,**features**,**glue**,**tags** etc.

* **step_definitions**
   - Step Definition classes are in step_definitons package keeps respective behavioral steps regarding Feature File which is in resources directory.

* **utilities**
   - Utilities package in which I store **ConfigurationReader**(to read urls,credentials,keys and etc. from configuration.properties) and I can store other classes like ApiUtilities has common ready method that I’m implementing during creating test scripts but this project I didn't create.

* **resources**
   - docs (To hold some docs)
   - features
     - Feature File represents the scenarios written in Gherkin language from end-user perspective in a single line structure.
   
* **configuration.properties**
   - Configuration properties file is keeping data like urls,credentials,keys,browser type(if we are dealing with browsers) based on key and value structure.

* **.gitlab-ci.yml**
   - This is a YAML file automatically runs whenever I push a commit to the server. This triggers a notification to the runner, and then it processes the series of tasks I specified in pipeline and add HTML reports as artifacts.

* **pom.xml**
   - **Project Object Model(POM)** Files are XML file that contains information related to the project and configuration information such as dependencies, source directory, plugin, goals etc. used by Maven to build the project.

- **Maven** is used as a build managemet tool and it helps me to manage the dependencies. It gives me the option that I can execute my scripts from terminal or from a CI tool.

- **Rest-Assured** is used to test out REST based services. I can validate and/or verify the responses of requests.

- **Git&Gitlab** is used as VCS.


### Gitlab CI 

* Gitlab is used as CI tool to automate and execute my framework. I created a **.gitlab-ci.yml** file and define my steps to run framework in pipeline and takes the generated HTML reports. 

### Structure of implementing

- Created the requests in **POSTMAN** using ```GET```, ```POST```, ```PUT```, ```DEL``` for the **CRUD** methods. 
- I create a collection from these requests then I created my framework in Intellij with maven. 
- I added all dependencies,plugins that I'll use and I create the framework structure based on POM structure with POJO classes. 
- I completed all covered scenarios and push my framework to **Gitlab** account. And I used **Gitlab CI** to execute whole framework from the gitlab as pipeline and generate fancy report.

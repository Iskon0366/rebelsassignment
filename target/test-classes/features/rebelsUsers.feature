@integration @users
Feature: Pet Store API Testing

  @createUser #POST --> Create a new user (/user)
  Scenario Outline: User should be able to create a new user with given info
    Given User should be able to create a User Pojo Object
      | id         | <id>         |
      | username   | <username>   |
      | firstName  | <firstName>  |
      | lastName   | <lastName>   |
      | email      | <email>      |
      | password   | <password>   |
      | phone      | <phone>      |
      | userStatus | <userStatus> |
    When User should send POST request to create a user with info below "user"
    Then Status code should be 200 in response User
    Then content type should be "application/json" in response User
    Then code value in the body must be 200 in response User
    Examples:
      | id | username | firstName  | lastName  | email  | password  | phone  | userStatus |
      | 1  | Chris    | firstName1 | lastName1 | email1 | password1 | phone1 | 111        |
      | 2  | Mike     | firstName2 | lastName2 | email2 | password2 | phone2 | 222        |
      | 3  | John     | firstName3 | lastName3 | email3 | password3 | phone3 | 333        |

  @createUserWithArray #POST --> Create new users(2) with array, with a json body (/user/createWithArray)
  Scenario Outline: User should be able to create two new user in an array with given info
    Given User should be able to create an array of users with given info
      | id         | <id>         |
      | username   | <username>   |
      | firstName  | <firstName>  |
      | lastName   | <lastName>   |
      | email      | <email>      |
      | password   | <password>   |
      | phone      | <phone>      |
      | userStatus | <userStatus> |
    When User should send POST request to create two user with array "user/createWithArray"
    Then Status code should be 200 in response User
    Then content type should be "application/json" in response User
    Then code value in the body must be 200 in response User
    Examples:
      | id | username | firstName  | lastName  | email  | password  | phone  | userStatus |
      | 4  | Dirk     | firstName4 | lastName4 | email4 | password4 | phone4 | 444        |

  @getUserByName #GET --> Get users by their names (/user/{username}) [Johny is invalid input]
  Scenario Outline: User should able to GET users by passing their names [Johny is invalid input]
    Given User should send a GET request with given username "<username>"
    Then Status code should be 200 in response User
    Then content type should be "application/json" in response User
    Then "username" value in the body must be "<username>" in response User
    Examples:
      | username |
      | Johny    |
      | Dirk     |
      | Kuzzie   |

  @updateUser #PUT --> Updated user with username (/user/{username})
  Scenario Outline: User should be able to Update users by their names
    Given User should send a GET request with given username "<username>"
    And User should create a Pojo Object from response and make some changes
    When User should send a PUT request with previous object to end point "user"
    Then Status code should be 200 in response User
    Then content type should be "application/json" in response User
    Examples:
      | username |
      | John     |

  @deleteUser #DELETE --> Delete user with username (/user/{username})
  Scenario Outline: User should be able to Delete users by their names
    Given User should send a DELETE request with given username "<username>"
    Then Status code should be 200 in response User
    Then content type should be "application/json" in response User
    Examples:
      | username |
      | Dirk     |

  @deleteUser #DELETE --> Delete the same user with username (/user/{username})
  Scenario Outline: User should NOT be able to Delete same users by their names
    Given User should send a DELETE request with given username "<username>"
    Then Status code should be 404 in response User
    Then response must not has a body
    Examples:
      | username |
      | Dirk     |

  @login #GET --> Logs user into the system with username and password (/user/login)
  Scenario Outline: User should be able to login the system with username and password, with Hamcrest
    Given User should send a GET request with given username "<username>" and "<password>" and verify status code, code,message
    Examples:
      | username | password |
      | Dirk     | 123456   |
      | Kuzzie   | 2345678  |

  @login #GET --> Logs user into the system without username and password (/user/login)
  Scenario: User should NOT be able to login the system without username and password
    Given User should send a GET request to "user/login"
    Then Status code should NOT be 200 in response User

  @logout #GET --> Logs out current logged in user session (/user/logout)
  Scenario: User should be able to logout current logged in user session
    Given User should send a GET request to "user/logout"
    Then Status code should be 200 in response User
    Then response message must match with given "ok" in response User


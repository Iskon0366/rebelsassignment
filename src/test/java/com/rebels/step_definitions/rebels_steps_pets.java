package com.rebels.step_definitions;

import com.rebels.pojos.*;
import com.rebels.utils.ConfigurationReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class rebels_steps_pets {


    static Pets pet1;

    String baseURI = ConfigurationReader.get("baseURI");

    Response response;

    String jsonBodyInvalid;

    static Response responseInvalid;

    int idToUpdate;

    Response responseUpdated;

    List<String> listOfStatus;

    Order order;

    static Response responseOrder;

    static Users user;

    static Response responseUser;

    String jsonBody;

    @Given("User should be able to create a Pet Pojo Object")
    public void user_should_be_able_to_create_a_Pet_Pojo_Object(Map<String,String> pets) {

        Tag tag = new Tag(Integer.parseInt(pets.get("tagsId")), pets.get("tagsName"));
        Category category = new Category(Integer.parseInt(pets.get("categoryId")), pets.get("categoryName"));

        List<String> photoUrlList = new ArrayList<>();
        photoUrlList.add(pets.get("photoUrl"));

        List<Tag> tagList = new ArrayList<>();
        tagList.add(tag);

        int id = Integer.parseInt(pets.get("id"));
        String name = pets.get("name");
        String status = pets.get("status");

        //I created Pojo object
        pet1 = new Pets(id,category,name,photoUrlList,tagList,status);
        System.out.println("pet1 = " + pet1.toString());
    }

    @When("User should send POST request to endpoint {string} with info below")
    public void user_should_send_POST_request_to_endpoint_with_info_below(String path) {

         response = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .and().body(pet1)
                .post(baseURI + path);
         response.prettyPrint();
    }

    @Then("Status code should be {int}")
    public void status_code_should_be(int str) {
        System.out.println("Response status code= " + response.getStatusCode());
        Assert.assertEquals(str,response.statusCode());
    }

    @Then("content type should be {string}")
    public void content_type_should_be(String str) {
        System.out.println("Response content type= " + response.getContentType());
        Assert.assertEquals(str,response.contentType());
    }

    @Then("Verify the given name is created")
    public void verify_the_given_name_is_created() {
        System.out.println("response.path(\"name\") = " + response.path("name"));
        Assert.assertEquals(pet1.getName(),response.body().path("name"));
    }

    @Given("User should be able to create a Pet Json String with invalid id to send POST request")
    public void user_should_be_able_to_create_a_pet_json_string_with_invalid_id_to_send_post_request(Map<String,String> jsonMap) {

        System.out.println("jsonMap.get(\"id\") = " + jsonMap.get("id"));

        jsonBodyInvalid = "{\n" +
                "  \"id\": " + jsonMap.get("id") + ",\n" +
                "  \"category\": {\n" +
                "    \"id\": " + Integer.parseInt(jsonMap.get("categoryId")) + ",\n" +
                "    \"name\": \"" + jsonMap.get("categoryName") + "\"\n" +
                "  },\n" +
                "  \"name\": \"" + jsonMap.get("name") + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"" + jsonMap.get("photoUrl") + "\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": " + Integer.parseInt(jsonMap.get("tagsId")) + ",\n" +
                "      \"name\": \"" + jsonMap.get("tagsName") + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"" + jsonMap.get("status") + "\"\n" +
                "}";
    }

    @When("User should send Pet Json String with POST request to endpoint {string} with info below")
    public void userShouldSendPetJsonStringWithPOSTRequestToEndpointWithInfoBelow(String endPoint) {

        responseInvalid = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .and().body(jsonBodyInvalid).post(baseURI + endPoint);

        responseInvalid.prettyPrint();
    }

    @Then("Status code should NOT be {int}")
    public void status_code_should_not_be(int statusCode) {
        System.out.println("responseInvalid.getStatusCode() = " + responseInvalid.getStatusCode());
        Assert.assertNotEquals(statusCode,responseInvalid.getStatusCode());
    }

    @Given("User should be able to create a Pet Json String with invalid photoUrl to send POST request")
    public void user_should_be_able_to_create_a_pet_json_string_with_invalid_photoUrl_to_send_post_request(Map<String,String> jsonMap) {

        System.out.println("jsonMap.get(\"photoUrl\") = " + jsonMap.get("photoUrl"));

        jsonBodyInvalid = "{\n" +
                "  \"id\": " + jsonMap.get("id") + ",\n" +
                "  \"category\": {\n" +
                "    \"id\": " + jsonMap.get("categoryId") + ",\n" +
                "    \"name\": \"" + jsonMap.get("categoryName") + "\"\n" +
                "  },\n" +
                "  \"name\": \"" + jsonMap.get("name") + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    " + Integer.parseInt(jsonMap.get("photoUrl")) + "\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": " + jsonMap.get("tagsId") + ",\n" +
                "      \"name\": \"" + jsonMap.get("tagsName") + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"" + jsonMap.get("status") + "\"\n" +
                "}";
    }


    @Given("User should take an Id of previous created Pet")
    public void user_should_take_an_id_of_previous_created_pet() {
        idToUpdate = pet1.getId(); //It holds last created Pet in 1st scenario "16"
        System.out.println("idToUpdate = " + idToUpdate);
        System.out.println("Pet will be updated = " + pet1.toString());
    }


    @When("User sends the Pet Pojo Object with PUT request by adding taken Id")
    public void user_sends_the_pet_pojo_object_with_put_request_by_adding_taken_Id() {
        pet1.setId(idToUpdate);
        responseUpdated = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .and().body(pet1).put(baseURI + "pet");

        responseUpdated.prettyPrint();
    }

    @Then("Previous Pet should be updated")
    public void previous_pet_should_be_updated() {

        Pets pet2 = new Pets();
        pet2 = responseUpdated.body().as(Pets.class);
        Assert.assertEquals("Verify the Pet is updated with taken Id",idToUpdate,(int)pet2.getId());
    }

    @Given("User should send a GET request with given {int}")
    public void user_should_send_a_get_request_with_given(Integer id) {
        response = RestAssured.given().pathParam("id", id).when().get(baseURI + "pet/{id}");
        response.prettyPrint();
    }

    @Then("User should verify the respective ids' of Pets {int}")
    public void userShouldVerifyTheRespectiveIdsOfPetsId(int id) {

        Assert.assertEquals("Verify the given ids are matching with response body",id,(int)response.path("id"));
    }


    @Given("User should send a GET request with given {string}")
    public void userShouldSendAGETRequestWithGiven(String status) {

        response = RestAssured.given().accept(ContentType.JSON)
                .and().queryParam("status",status)
                .when().get(baseURI+"pet/findByStatus");

        listOfStatus = response.body().path("status");
        System.out.println("listOfStatus = " + listOfStatus);
    }

    @Then("Verify the response status is matching with the given {string}")
    public void verifyTheResponseStatusIsMatchingWithTheGiven(String status) {

        for (String singleStatus : listOfStatus ) {
            Assert.assertEquals("Verify status' are matching",status,singleStatus);
        }
    }

    @Given("User should send a GET request with given {string},{string}")
    public void userShouldSendAGETRequestWithGiven(String status1, String status2) {

        response = RestAssured.given().accept(ContentType.JSON)
                .and().queryParam("status",status1+","+status2)
                .when().get(baseURI+"pet/findByStatus");

        listOfStatus = response.body().path("status");
        System.out.println("listOfStatus = " + listOfStatus);

    }

    @Then("Verify the response status is matching with the given {string},{string}")
    public void verifyTheResponseStatusIsMatchingWithTheGiven(String status1, String status2) {

        int count = 0;

        for (String singleStatus : listOfStatus) {
            if (!singleStatus.equals(status1) && !singleStatus.equals(status2)) {
                count = 1;
                break;
            }
        }

        Assert.assertEquals("Verify response has one of the status that is given",0,count);
    }


    @Given("User should send a GET request with given {string},{string} without comma")
    public void userShouldSendAGETRequestWithGivenWithoutComma(String status1, String status2) {

        responseInvalid = RestAssured.given().accept(ContentType.JSON)
                .and().queryParam("status",status1+" "+status2)
                .when().get(baseURI+"pet/findByStatus");

        System.out.println("Body of the response that two status without comma : ");
        responseInvalid.prettyPrint();
    }

    @Then("code value in the body must be {int}")
    public void codeValueInTheBodyMustBe(int codeValue) {

        Assert.assertEquals("Verify the code value in the body",codeValue,(int)response.body().path("code"));
    }

    @And("message value in the body must be {int}")
    public void messageValueInTheBodyMustBeId(int id) {

        Assert.assertEquals("Verify the message value in the body",Integer.toString(id),response.body().path("message"));
    }

    @Given("User should send a POST with given form data {string},{string},{int} to the created Pets")
    public void userShouldSendAPOSTWithGivenFormDataToTheCreatedPets(String name, String status, int id) {

        response = RestAssured.given().accept(ContentType.JSON)
                .and().pathParam("id",id).formParam("name",name)
                .formParam("status",status)
                .post(baseURI+"pet/{id}");

        response.prettyPrint();
    }

    @And("previous {int} pet should be updated with given {string} and {string}")
    public void previousIdPetShouldBeUpdatedWithGivenAnd( int id, String name, String status) {

        Assert.assertEquals("Verify the Pet is updated with taken Id",id,(int)response.path("id"));
        Assert.assertEquals("Verify the Pet is updated with taken name",name,response.path("name"));
        Assert.assertEquals("Verify the Pet is updated with taken status",status,response.path("status"));
    }

    @Given("User should send a POST with given form data {int},{int},{int} to the created Pets")
    public void userShouldSendAPOSTWithGivenFormDataNameStatusIdToTheCreatedPets(int name, int status, int id) {

        response = RestAssured.given().accept(ContentType.JSON)
                .and().pathParam("id",id).formParam("name",name)
                .formParam("status",status)
                .post(baseURI+"pet/{id}");

        response.prettyPrint();
    }

    @Given("User should send a DELETE request with given {int}")
    public void userShouldSendADELETERequestWithGivenId(int id) {

        response = RestAssured.given().contentType(ContentType.JSON)
                .and().pathParam("id",id).delete(baseURI+"pet/{id}");

        response.prettyPrint();
    }

    @Then("And message value in the body must be {string}")
    public void andMessageValueInTheBodyMustBe(String message) {

        Assert.assertEquals("Verify the message value in the body",message,response.body().path("message"));
    }

    @Given("User should be able to create an Order with given info")
    public void userShouldBeAbleToCreateAnOrderWithGivenInfo(Map<String,String> orders) {

        order = new Order(Integer.parseInt(orders.get("id")),Integer.parseInt(orders.get("petId")),Integer.parseInt(orders.get("quantity")),orders.get("shipDate"),orders.get("status"),Boolean.parseBoolean(orders.get("complete")));
        System.out.println("order.toString() = " + order.toString());
    }

    @When("User sends POST request to place an order for a pet to {string}")
    public void userSendsPOSTRequestToPlaceAnOrderForAPetTo(String endPoint) {

        responseOrder = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .body(order).post(baseURI + endPoint);

        responseOrder.prettyPrint();
    }

    @Then("Status code should be {int} for order response")
    public void statusCodeShouldBeForOrderResponse(int statusCode) {

        System.out.println("ResponseOder status code= " + responseOrder.getStatusCode());
        Assert.assertEquals(statusCode,responseOrder.statusCode());
    }

    @Then("content type should be {string} for order response")
    public void contentTypeShouldBeForOrderResponse(String contentType) {

        System.out.println("Response content type= " + responseOrder.getContentType());
        Assert.assertEquals(contentType,responseOrder.contentType());
    }


    @Given("User should be able to send a POST with given {int} to {string}")
    public void userShouldBeAbleToSendAPOSTWithGivenIdTo(int id, String endPoint) {

        responseOrder = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .pathParam("id",id)
                .get(baseURI + endPoint+"{id}");

        responseOrder.prettyPrint();
    }

    @Then("response id must match with given {int}")
    public void responseIdMustMatchWithGivenId(int id) {

        Assert.assertEquals(id,(int)responseOrder.body().path("id"));

    }

    @Given("User should be able to send a DELETE with given {int} to {string}")
    public void userShouldBeAbleToSendADELETEWithGivenIdTo(int id, String endPoint) {

        responseOrder = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .pathParam("id",id)
                .delete(baseURI + endPoint+"{id}");

        responseOrder.prettyPrint();

    }

    @Then("response message must match with given {string}")
    public void responseMessageMustMatchWithGivenId(String str) {

        Assert.assertEquals("Verify the message value in the body",str,responseOrder.body().path("message"));
    }

    @Given("User should be able to send a GET with given path {string}")
    public void userShouldBeAbleToSendAGETWithGivenPath(String endPoint) {

        responseOrder = RestAssured.given().contentType(ContentType.JSON)
                .get(baseURI+endPoint);

        responseOrder.prettyPrint();
    }


    @Given("User should be able to create a User Pojo Object")
    public void userShouldBeAbleToCreateAUserPojoObject(Map<String,String> userMap) {

        user = new Users(Integer.parseInt(userMap.get("id")), userMap.get("username"), userMap.get("firstName"), userMap.get("lastName"), userMap.get("email"), userMap.get("password"), userMap.get("phone"), Integer.parseInt(userMap.get("userStatus")));

    }

    @When("User should send POST request to create a user with info below {string}")
    public void userShouldSendPOSTRequestToCreateAUserWithInfoBelow(String endPoint) {

        responseUser = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .and().body(user).post(baseURI + endPoint);

        responseUser.prettyPrint();
    }

    @Then("Status code should be {int} in response User")
    public void statusCodeShouldBeInResponseUser(int statusCode) {
        Assert.assertEquals("Verify status code in User response",statusCode,responseUser.getStatusCode());
    }

    @Then("content type should be {string} in response User")
    public void contentTypeShouldBeInResponseUser(String content) {
        Assert.assertEquals("Verify content type in User response",content,responseUser.getContentType());

    }

    @Then("code value in the body must be {int} in response User")
    public void codeValueInTheBodyMustBeInResponseUser(int codeValue) {
        Assert.assertEquals("Verify code value in User response",codeValue,(int)responseUser.path("code"));
    }


    @Given("User should be able to create an array of users with given info")
    public void userShouldBeAbleToCreateAnArrayOfUsersWithGivenInfo(Map<String,String> userMap) {

        jsonBody = "[\n" +
                "  {\n" +
                "    \"id\": "+Integer.parseInt(userMap.get("id"))+",\n" +
                "    \"username\": \""+userMap.get("username")+"\",\n" +
                "    \"firstName\": \""+userMap.get("firstName")+"\",\n" +
                "    \"lastName\": \""+userMap.get("lastName")+"\",\n" +
                "    \"email\": \""+userMap.get("email")+"\",\n" +
                "    \"password\": \""+userMap.get("password")+"\",\n" +
                "    \"phone\": \""+userMap.get("phone")+"\",\n" +
                "    \"userStatus\": "+Integer.parseInt(userMap.get("userStatus"))+"\n" +
                "  },\n" +
                "  {\n" +
                "    \"id\": 5,\n" +
                "    \"username\": \"Kuzzie\",\n" +
                "    \"firstName\": \"string\",\n" +
                "    \"lastName\": \"string\",\n" +
                "    \"email\": \"string\",\n" +
                "    \"password\": \"string\",\n" +
                "    \"phone\": \"string\",\n" +
                "    \"userStatus\": 5\n" +
                "  }\n" +
                "]";
    }

    @When("User should send POST request to create two user with array {string}")
    public void userShouldSendPOSTRequestToCreateTwoUserWithArray(String endPoint) {

        responseUser = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .and().body(jsonBody).post(baseURI + endPoint);

        responseUser.prettyPrint();

    }


    @Given("User should send a GET request with given username {string}")
    public void userShouldSendAGETRequestWithGivenUsername(String username) {

        responseUser = RestAssured.given().accept(ContentType.JSON).and()
                .pathParam("username", username).
                        when().get(baseURI + "user/{username}");
        responseUser.prettyPrint();

    }

    @Then("{string} value in the body must be {string} in response User")
    public void valueInTheBodyMustBeUsernameInResponseUser(String path, String username) {

        System.out.println("responseUser name= " + responseUser.path(path));
        Assert.assertEquals("Verify the names are matching",username,responseUser.path(path));

    }

    @And("User should create a Pojo Object from response and make some changes")
    public void userShouldCreateAPojoObjectFromResponseAndMakeSomeChanges() {

        user = responseUser.body().as(Users.class);
        user.setId(9);
        user.setEmail("John@gmail.com");
        user.setUserStatus(999);

        System.out.println("user.toString() = " + user.toString());
    }

    @When("User should send a PUT request with previous object to end point {string}")
    public void userShouldSendAPUTRequestWithPreviousObjectToEndPoint(String endPoint) {

        responseUser = RestAssured.given().accept(ContentType.JSON).contentType(ContentType.JSON)
                .and().pathParam("username",user.getUsername())
                .and().body(user).when().put(baseURI+endPoint+"/{username}");

        responseUser.prettyPrint();

    }

    @Given("User should send a DELETE request with given username {string}")
    public void userShouldSendADELETERequestWithGivenUsername(String username) {

        responseUser = RestAssured.given().accept(ContentType.JSON)
                .and().pathParam("username",username)
                .when().delete(baseURI+"user/{username}");

        responseUser.prettyPrint();

    }

    @Then("response must not has a body")
    public void responseMustNotHasABody() {

        Assert.assertEquals("Verify the response body is empty",0, responseUser.body().asString().length());

    }


    @Given("User should send a GET request with given username {string} and {string} and verify status code, code,message")
    public void userShouldSendAGETRequestWithGivenUsernameAndAndVerifyStatusCodeCodeMessage(String username, String password) {

        RestAssured.given().header("Accept","application/json").contentType(ContentType.JSON)
                .and().queryParam("username",username)
                .queryParam("password",password)
                .when().get(baseURI+"user/login").then()
                .assertThat().statusCode(200).contentType(ContentType.JSON)
                .header("Content-Type",Matchers.equalTo("application/json"))
                .body("code", Matchers.equalTo(200)
                ,"message",Matchers.startsWith("logged in user")).log().all();

    }

    @Given("User should send a GET request to {string}")
    public void userShouldSendAGETRequestTo(String endPoint) {

        responseUser = RestAssured.given().accept(ContentType.JSON).when().get(baseURI + endPoint);
        responseUser.prettyPrint();
    }

    @Then("Status code should NOT be {int} in response User")
    public void statusCodeShouldNOTBeInResponseUser(int statusCode) {

        Assert.assertNotEquals("Verify the status code is not 200",statusCode,responseUser.getStatusCode());

    }

    @Then("response message must match with given {string} in response User")
    public void responseMessageMustMatchWithGivenInResponseUser(String message) {

        Assert.assertEquals("Verify the message value in the body",message,responseUser.body().path("message"));

    }

    @Given("User should send a POST request to {string}")
    public void userShouldSendAPOSTRequestTo(String path) {

        responseUser = RestAssured.given().accept(ContentType.JSON).contentType("multipart/form-data")
                .and().multiPart("file", new File("src/test/resources/docs/leauw.png"))
                .multiPart("additionalMetadata","I added a 'leeuw' image")
                .when().post(baseURI + path);

        responseUser.prettyPrint();

    }

    @Given("User should send a POST request to {string} without a image file")
    public void userShouldSendAPOSTRequestToWithoutAImageFile(String path) {

        responseUser = RestAssured.given().accept(ContentType.JSON).contentType("multipart/form-data")
                .and().multiPart("file","string_image")
                .multiPart("additionalMetadata","I added a only string")
                .when().post(baseURI + path);

        responseUser.prettyPrint();

    }
}

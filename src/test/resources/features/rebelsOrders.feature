@integration @orders
Feature: Pet Store API Testing - Orders

  Scenario Outline: User should be able to place an order for a pet
    Given User should be able to create an Order with given info
      | id       | <id>       |
      | petId    | <petId>    |
      | quantity | <quantity> |
      | shipDate | <shipDate> |
      | status   | <status>   |
      | complete | <complete> |
    When User sends POST request to place an order for a pet to "store/order"
    Then Status code should be 200 for order response
    Then content type should be "application/json" for order response
    Examples:
      | id | petId | quantity | shipDate   | status    | complete |
      | 1  | 11    | 3        | 2020-12-13 | placed    | true     |
      | 2  | 12    | 6        | 2020-12-16 | approved  | false    |
      | 3  | 13    | 9        | 2020-12-19 | delivered | true     |

   #GET --> Find purchase order by ID (/store/order/{orderId})
  Scenario Outline: User should be able to find an order by <id>
    Given User should be able to send a POST with given <id> to "store/order/"
    Then Status code should be 200 for order response
    Then response id must match with given <id>
    Examples:
      | id |
      | 1  |
      | 2  |
      | 3  |

   #DELETE --> Delete an order by ID (/store/order/{orderId})
  Scenario Outline: User should be able to delete an order by <id>
    Given User should be able to send a DELETE with given <id> to "store/order/"
    Then Status code should be 200 for order response
    Then response message must match with given "<id>"
    Examples:
      | id |
      | 1  |
      | 2  |

  #DELETE --> Delete an existing order by ID (/store/order/{orderId})
  Scenario Outline: User should NOT be able to delete an order by <id>
    Given User should be able to send a DELETE with given <id> to "store/order/"
    Then Status code should be 404 for order response
    Then response message must match with given "Order Not Found"
    Examples:
      | id |
      | 1  |
      | 2  |


  #GET --> Return pet inventories by status (/store/inventory)
  Scenario: User should be able to GET pet inventories by status
    Given User should be able to send a GET with given path "store/inventory"
    Then Status code should be 200 for order response




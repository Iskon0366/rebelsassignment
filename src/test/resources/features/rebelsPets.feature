@integration @pets
Feature: Pet Store API Testing

  @createPet #POST --> Add a new pet to the store(/pet)
  Scenario Outline: User should be able to create a PET with given info
    Given User should be able to create a Pet Pojo Object
      | id           | <id>           |
      | name         | <name>         |
      | status       | <status>       |
      | categoryId   | <categoryId>   |
      | categoryName | <categoryName> |
      | photoUrl     | <photoUrl>     |
      | tagsId       | <tagsId>       |
      | tagsName     | <tagsName>     |
    When User should send POST request to endpoint "pet" with info below
    Then Status code should be 200
    Then content type should be "application/json"
    Then Verify the given name is created
    Examples:
      | id | name    | status    | categoryId | categoryName | photoUrl  | tagsId | tagsName  |
      | 11 | Kangie  | avaliable | 333        | cat1         | photoUrl1 | 1      | tagsName1 |
      | 12 | Charlie | pending   | 444        | cat1         | photoUrl2 | 1      | tagsName1 |
      | 13 | Max     | sold      | 555        | cat1         | photoUrl3 | 2      | tagsName2 |
      | 14 | Buddy   | avaliable | 666        | cat1         | photoUrl4 | 2      | tagsName2 |
      | 15 | Oscar   | sold      | 777        | cat1         | photoUrl5 | 3      | tagsName3 |
      | 16 | Milo    | pending   | 888        | cat1         | photoUrl6 | 3      | tagsName3 |


  @createPetWithInvalid #POST --> Add a new pet to the store(/pet) --> #Negative scenario with invalid id
  Scenario Outline: User should not be able to create a PET with invalid id parameter
    Given User should be able to create a Pet Json String with invalid id to send POST request
      | id           | <id>           |
      | name         | <name>         |
      | status       | <status>       |
      | categoryId   | <categoryId>   |
      | categoryName | <categoryName> |
      | photoUrl     | <photoUrl>     |
      | tagsId       | <tagsId>       |
      | tagsName     | <tagsName>     |
    When User should send Pet Json String with POST request to endpoint "pet" with info below
    Then Status code should NOT be 200
    Examples:
      | id   | name   | status    | categoryId | categoryName | photoUrl  | tagsId | tagsName  |
      | Five | Kangie | avaliable | 333        | cat1         | photoUrl1 | 1      | tagsName1 |
      | Six  | Karlie | pending   | 444        | cat2         | photoUrl2 | 2      | tagsName2 |


  @createPetWithInvalidPhoto #POST --> Add a new pet to the store(/pet) --> #Negative scenario with photoUrl
  Scenario Outline: User should not be able to create a PET with invalid photoUrl parameter
    Given User should be able to create a Pet Json String with invalid photoUrl to send POST request
      | id           | <id>           |
      | name         | <name>         |
      | status       | <status>       |
      | categoryId   | <categoryId>   |
      | categoryName | <categoryName> |
      | photoUrl     | <photoUrl>     |
      | tagsId       | <tagsId>       |
      | tagsName     | <tagsName>     |
    When User should send Pet Json String with POST request to endpoint "pet" with info below
    Then Status code should NOT be 200
    Examples:
      | id | name     | status    | categoryId | categoryName | photoUrl | tagsId | tagsName  |
      | 17 | Kangiele | avaliable | 333        | cat1         | 41       | 111    | tagsName1 |
      | 18 | Karliele | pending   | 444        | cat2         | 42       | 999    | tagsName2 |

  @updateExisting @wip #PUT Update an existing pet(/pet)
  Scenario Outline: User should be able to update an existing Pet with PUT request sending Pojo Object
    Given User should take an Id of previous created Pet
    And User should be able to create a Pet Pojo Object
      | id           | <id>           |
      | name         | <name>         |
      | status       | <status>       |
      | categoryId   | <categoryId>   |
      | categoryName | <categoryName> |
      | photoUrl     | <photoUrl>     |
      | tagsId       | <tagsId>       |
      | tagsName     | <tagsName>     |
    When User sends the Pet Pojo Object with PUT request by adding taken Id
    Then Previous Pet should be updated
    Examples:
      | id | name  | status    | categoryId | categoryName | photoUrl | tagsId | tagsName  |
      | 0  | Cillo | avaliable | 333        | cat1         | 41       | 111    | tagsName1 |


  @getById #GET --> Find pet by Id(/pet/{petId})
  Scenario Outline: User should be able to get a Pet with GET request by sending Id
    Given User should send a GET request with given <id>
    Then Status code should be 200
    Then content type should be "application/json"
    Then User should verify the respective ids' of Pets <id>
    Examples:
      | id |
      | 14 |
      | 15 |
      | 16 |


  @findByStatus #GET --> Find pets by status (/pet/findByStatus)
  Scenario Outline: User should able to get Pets by passing status info
    Given User should send a GET request with given "<status>"
    Then Status code should be 200
    Then content type should be "application/json"
    Then Verify the response status is matching with the given "<status>"
    Examples:
      | status    |
      | pending   |
      | sold      |
      | avaliable |


  @findByTwoStatus #GET --> Find pets by two of status (/pet/findByStatus)
  Scenario Outline: User should able to get Pets by passing two status info
    Given User should send a GET request with given "<status1>","<status2>"
    Then Status code should be 200
    Then content type should be "application/json"
    Then Verify the response status is matching with the given "<status1>","<status2>"
    Examples:
      | status1   | status2   |
      | pending   | sold      |
      | sold      | available |
      | avaliable | pending   |


  @findByTwoStatusWithoutComma #GET --> Find pets by two of status without comma (/pet/findByStatus)
  Scenario Outline: User should able to get Pets by passing two status info without comma
    Given User should send a GET request with given "<status1>","<status2>" without comma
    Then Status code should NOT be 200

    Examples:
      | status1   | status2   |
      | pending   | sold      |
      | sold      | available |
      | avaliable | pending   |

  @updateWithFormdata #POST --> Updates a pet in the store with form data (pet/{petId})
  Scenario Outline: User should update a pet in the store with form data
    Given User should send a POST with given form data "<name>","<status>",<id> to the created Pets
    Then Status code should be 200
    Then code value in the body must be 200
    And message value in the body must be <id>
    Given User should send a GET request with given <id>
    And previous <id> pet should be updated with given "<name>" and "<status>"
    Examples:
      | name  | status    | id |
      | yogie | available | 15 |
      | zogie | pending   | 16 |


  @updateWithInvalidFormdata #POST --> Updates a pet in the store with invalid form data (pet/{petId})
  Scenario Outline: User should NOT update a pet in the store with invalid form data
    Given User should send a POST with given form data <name>,<status>,<id> to the created Pets
    Then Status code should NOT be 200
    Examples:
      | name  | status | id |
      | 12345 | 67890  | 17 |
      | 00000 | 00000  | 18 |


  @deleteWithId #DELETE --> Deletes an existing (pet /pet/{petId})
  Scenario Outline: User should delete a pet in the store with id -> <id>
    Given User should send a DELETE request with given <id>
    Then Status code should be 200
    Then code value in the body must be 200
    And message value in the body must be <id>
    Then User should send a GET request with given <id>
    Then Status code should be 404
    Then And message value in the body must be "Pet not found"
    Examples:
      | id |
      | 11 |
      | 12 |

  @uploadImage #POST --> Upload an image (/pet/{petId}/uploadImage)
  Scenario: User should be able to upload an image
    Given User should send a POST request to "pet/14/uploadImage"
    Then Status code should be 200 in response User


  @uploadImage #POST --> Upload an image without image (/pet/{petId}/uploadImage)
  Scenario: User should NOT be able to upload an image without a file
    Given User should send a POST request to "pet/14/uploadImage" without a image file
    Then Status code should NOT be 200 in response User

